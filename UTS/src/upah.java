package UTS;
import java.util.Scanner;
public class upah {
    public static void main(String[] args) {
        String Nama;
        char Gol;
        int UpahPerJam;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan Nama Karyawan: ");
        Nama = in.nextLine();
        System.out.print("Masukkan Golongan Karyawan: ");
        Gol = in.next().charAt(0); // charAt(0) to get the first character
        if (Gol == 'A') {
            UpahPerJam = 10000;
        } else if (Gol == 'B') {
            UpahPerJam = 15000;
        } else if (Gol == 'C') {
            UpahPerJam = 20000;
        } else if (Gol == 'D') {
            UpahPerJam = 25000;
        } else {
            UpahPerJam = 0;
        }
        System.out.println(Nama + ", UpahPerJam kamu Adalah Rp. " + UpahPerJam);
        in.close();
    }
}
