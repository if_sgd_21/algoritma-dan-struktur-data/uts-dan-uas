package UTS;

class Kampus {
    String name;
    String status;
    int tahun;
    Kampus (){}
    Kampus(String name, String status) {
        this.name = name;
        this.status = status;
    }
    void display(String name, String status) {
        System.out.println("Nama: " + name);
        System.out.println("Status: " + status);
    }
    void display(int tahun) {
        System.out.println("Tahun: " + tahun);
    }
}
class UIN extends Kampus {
    UIN(String name, String status) {
        super(name, status);
    }
    void display(String name, String status)
    {
        System.out.println("Nama Kampus: " + name + ", Status: " + status);
    }
    void display(int tahun)
    {
        System.out.println("Tahun Berdiri: " + tahun);
    }
}

class UPI extends Kampus {
    UPI(String name, String status) {
        super(name, status);
    }
    void display(String name, String status)
    {
        System.out.println("Nama Kampus: " + name + ", Status: " + status);
    }
    void display(int tahun)
    {
        System.out.println("Tahun Berdiri: " + tahun);
    }

}
public class Polymorphism {
    public static void main(String[] args) {
        Kampus kampus = new Kampus();
        //polymorphism, merujuk ke objek yang berbeda dengan tipe yang sama
        kampus = new UIN("UIN", "PTKIN");
        kampus.display("UIN", "PTKIN");
        //Overloading, method yang sama dengan parameter yang berbeda
        kampus.display(1968); //String menjadi int
        kampus = new UPI("UPI", "PTN");
        kampus.display("UPI", "PTN");
        kampus.display(1954);
    }
}