package UTS;
import java.util.Scanner;
public class stackArray {
    public static void main(String[] args) {
        int counter = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan jumlah stack yang diinginkan : ");
        int n = in.nextInt();
        Stack stk = new Stack();
        stk.n = n;
        stk.stack = new String[n];
        System.out.println("Pilih menu : ");
        System.out.println("1. Push");
        System.out.println("2. Pop");
        System.out.println("3. Show");
        System.out.println("4. Check Storage");
        System.out.println("5. Peek");
        System.out.println("6. Exit");
        int pilih = in.nextInt();
        while (pilih != 6)
        {
            switch (pilih)
            {
                case 1:
                    System.out.print("Berapa jumlah barang yang akan anda masukkan?" + "(Penyimpanan tersisa = " + (n - counter) + ") : ");
                    int x = in.nextInt();
                    if (x > n) 
                    {
                        System.out.println("Maaf, anda hanya dapat memasukkan maksimal " + n + " barang");
                    } else if (x > (n-counter)) {
                        System.out.println("Maaf, anda hanya dapat memasukkan maksimal " + (n-counter) + " barang");
                    } else if (x <= (n-counter)) {
                        for (int i = 0; i < x; i++)
                        {
                            System.out.print("Masukkan barang ke-" + (i+1) + " : ");
                            String data = in.next();
                            stk.push(data);
                            counter++;
                        }
                    }
                    else {
                        for (int i = 0; i < x; i++)
                        {
                            System.out.print("Masukkan barang ke-" + (i + 1) + " : ");
                            String data = in.next();
                            stk.push(data);
                            counter++;
                        }
                        System.out.println("Jumlah data yang bisa dimasukkan selanjutnya: " + (n - counter));
                    }
                    break;
                case 2:
                    System.out.print("Berapa barang yang ingin anda hapus : ");
                    int y = in.nextInt();
                    for (int i = 0; i < y; i++)
                    {
                        stk.pop();
                        counter--;
                    }
                    break;
                case 3:
                    stk.show();
                    break;
                case 4:
                    stk.checkStorage();
                    break;
                case 5:
                    stk.peek();
                    break;
                default:
                    System.out.println("Menu tidak tersedia");
                    break;
            }
            System.out.println("Pilih menu : ");
            System.out.println("1. Push");
            System.out.println("2. Pop");
            System.out.println("3. Show");
            System.out.println("4. Check Storage");
            System.out.println("5. Peek");
            System.out.println("6. Exit");
            pilih = in.nextInt();
        }
        in.close();
    }
}
