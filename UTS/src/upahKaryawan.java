package UTS;
import java.util.Scanner;
public class upahKaryawan {
    public static void main(String[] args) {
        String Nama;
        char Gol;
        int UpahPerJam;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan Nama Karyawan: ");
        Nama = in.nextLine();
        System.out.print("Masukkan Golongan Karyawan: ");
        Gol = in.next().charAt(0); // charAt(0) to get the first character
        while(Gol != 'A' && Gol != 'B' && Gol != 'C' && Gol != 'D'){
            System.out.println("Golongan yang anda masukkan salah!");
            System.out.print("Masukkan Golongan Karyawan: ");
            Gol = in.next().charAt(0);
        }
        if (Gol == 'A') {
            UpahPerJam = 10000;
        } else if (Gol == 'B') {
            UpahPerJam = 15000;
        } else if (Gol == 'C') {
            UpahPerJam = 20000;
        } else if (Gol == 'D') {
            UpahPerJam = 25000;
        } else {
            UpahPerJam = 0;
        }
        System.out.println(Nama + ", UpahPerJam kamu Adalah Rp. " + UpahPerJam);
        in.close();
    }
}
