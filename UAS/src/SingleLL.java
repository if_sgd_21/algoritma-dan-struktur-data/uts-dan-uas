//single Linked List java
public class SingleLL {
    class Node {
        Object data;
        Node next;
        public Node(Object data) {
            this.data = data;
            this.next = null;
        }
    }
    public Node head = null;
    public Node tail = null;
    public void add(Object data) {
        Node newNode = new Node(data);
        if(head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.next = newNode;
            tail = newNode;
        }
    }
    public void remove(Object data) {
        Node current = head;
        Node previous = null;
        if(head == null) {
            System.out.println("List kosong");
            return;
        }
        while(current != null) {
            if(current.data.equals(data)) {
                if(current == head) {
                    head = head.next;
                } else {
                    previous.next = current.next;
                }
                return;
            }
            previous = current;
            current = current.next;
        }
    }
    public void contains(Object data) {
        Node current = head;
        int i = 1;
        boolean flag = false;
        if(head == null) {
            System.out.println("List kosong");
            return;
        }
        while(current != null) {
            if(current.data.equals(data)) {
                flag = true;
                break;
            }
            i++;
            current = current.next;
        }
        if(flag) {
            System.out.println("Data " + data + " ditemukan pada indeks ke-" + i);
        } else {
            System.out.println("Data " + data + " tidak ditemukan");
        }
    }
    public void display() {
        Node current = head;
        if(head == null) {
            System.out.println("List kosong");
            return;
        }
        System.out.println("Data pada list : ");
        while(current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }
    //sort method for string
    public void sort() {
        Node current = head;
        Node index = null;
        Object temp;
        if(head == null) {
            return;
        } else {
            while(current != null) {
                index = current.next;
                while(index != null) {
                    if(current.data.toString().compareTo(index.data.toString()) > 0) {
                        temp = current.data;
                        current.data = index.data;
                        index.data = temp;
                    }
                    index = index.next;
                }
                current = current.next;
            }
        }
    }
}