import java.util.Scanner;
import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        //Panggil class Linked Listnya
        SingleLL list = new SingleLL();
        //Panggil class Scanner
        Scanner in = new Scanner(System.in);
        // System.out.println("==================================");
        // System.out.println("TUGAS UAS : Single Linked List");
        // System.out.println("Nama : Dian Saputra");
        // System.out.println("NIM : 1217050037");
        // System.out.println("Kelas : IF-A-21");
        // System.out.println("==================================");
        System.out.println("Silahkan pilih menu di bawah ini : ");
        System.out.println("1. Tambah Barang");
        System.out.println("2. Hapus Barang");
        System.out.println("3. Cari Barang");
        System.out.println("4. Urutkan Barang");
        System.out.println("5. Tampilkan Barang");
        System.out.println("6. Keluar");
        System.out.println("==================================");
        System.out.print("Pilihan anda : ");
        int pilihan = in.nextInt();
        System.out.println("==================================");
        while(pilihan != 6) {
            switch(pilihan) {
                case 1:
                    System.out.print("Masukkan nama barang : ");
                    String namaBarang = in.next();
                    list.add(namaBarang);
                    System.out.println("==================================");
                    System.out.println("Barang berhasil ditambahkan");
                    System.out.println("==================================");
                    break;
                case 2:
                    System.out.print("Masukkan nama barang : ");
                    String namaBarang2 = in.next();
                    list.remove(namaBarang2);
                    System.out.println("==================================");
                    System.out.println("Barang berhasil dihapus");
                    System.out.println("==================================");
                    break;
                case 3:
                    System.out.print("Masukkan nama barang : ");
                    String namaBarang3 = in.next();
                    list.contains(namaBarang3);
                    System.out.println("==================================");
                    break;
                case 4:
                    System.out.println("==================================");
                    System.out.println("Barang yang ada : ");
                    list.display();
                    System.out.println("==================================");
                    System.out.println("Barang yang sudah diurutkan : ");
                    list.sort();
                    list.display();
                    System.out.println("==================================");
                    break;
                case 5:
                    System.out.println("==================================");
                    System.out.println("Barang yang ada : ");
                    list.display();
                    System.out.println("==================================");
                    break;
                default:
                    System.out.println("==================================");
                    System.out.println("Pilihan tidak tersedia");
                    System.out.println("==================================");
                    break;
            }
            System.out.println("Silahkan pilih menu di bawah ini : ");
            System.out.println("1. Tambah Barang");
            System.out.println("2. Hapus Barang");
            System.out.println("3. Cari Barang");
            System.out.println("4. Urutkan Barang");
            System.out.println("5. Tampilkan Barang");
            System.out.println("6. Keluar");
            System.out.println("==================================");
            System.out.print("Pilihan anda : ");
            pilihan = in.nextInt();
            System.out.println("==================================");
        }
        System.out.println("==================================");
        System.out.println("Terima kasih telah menggunakan program ini");
        System.out.println("==================================");
    }
}
